/*
// ==================================================== //
//                    Copyright 2016                    //
//               Franciszek "Fgame" Olejnik             //
// ==================================================== //
// This work is licensed under a Creative Commons       //
//        Attribution 4.0 International License.        //
// ==================================================== //
//    Niniejsza praca jest licencjonowana zgodnie z     //
//       miedzynarodową licencja Creative Commons       //
//                "Uznanie autorstwa 4.0"               //
// ==================================================== //
*/

import java.util.Scanner;

public class Client {

    public static void main(String[] args) throws Exception {

        //wiadomość powitalna
        Console.setColor("yellow");
        Console.log("=============================================================");
        Console.log("      Client chatu - Autor: Franciszek \"Fgame\" Olejnik     ");
        Console.log("                        version 1.0.0                        ");
        Console.log("=============================================================");
        Console.resetColor();

        //tworzenie obiektu do obsługi consoli
        Scanner console = new Scanner(System.in);

        //wiadomośc powitalna
        Console.log("Client startuje...");

        //tworzenie buffera / zmiennej na port i ip
        String buffer = "";
        String ip = "";
        int port = 0;

        //pobieranie ip
        Console.log("Podaj ip serwera (v4 lub v6): ");
        while(true){
            buffer = console.next();
            if (DataValidator.isIp(buffer)) {
                ip = buffer;
                break;
            } else {
                Console.logError("Błedne ip! Spróbuj ponownie");
            }
        }

        //pobieranie portu
        Console.log("Podaj port (0-65535): ");
        while (true) {
            buffer = console.next();
            if (DataValidator.isPort(buffer)) {
                port = (Integer.parseInt(buffer));
                break;
            } else {
                Console.logError("Błedny port! Spróbuj ponownie");
            }
        }

        //tworzenie clientConstollera ( odpali sie on sam )
        ClientController client = new ClientController(ip,port);
    }
}
