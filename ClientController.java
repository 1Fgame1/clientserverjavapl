/*
// ==================================================== //
//                    Copyright 2016                    //
//               Franciszek "Fgame" Olejnik             //
// ==================================================== //
// This work is licensed under a Creative Commons       //
//        Attribution 4.0 International License.        //
// ==================================================== //
//    Niniejsza praca jest licencjonowana zgodnie z     //
//       miedzynarodową licencja Creative Commons       //
//                "Uznanie autorstwa 4.0"               //
// ==================================================== //
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ClientController {

    //watek dla wejscia
    private ClientThread clientIn;

    //watek dla wyjscia
    private ClientThread clientOut;

    //glowny buffer
    private String buffer;

    //glowny socket
    private Socket client;

    //obsluga klawiatury
    private Scanner console;


    //glowny system in / out
    private BufferedReader mainIn;
    private PrintWriter mainOut;

    //status polaczenia
    //1 dla dzialajacego servera
    //0 dla servera ktory ma zakonczyc prace
    private boolean status;


    //konstruktor
    public ClientController(String ip,int port){

        //tworzenie scannera
        console = new Scanner(System.in);

        try {

            //tworzenie socketu
            this.client = new Socket(ip,port);

        } catch(Exception e) {
            //błąd
            Console.logError("Nieudało się połaczyć z serwerem");
            Console.log("Kończenie pracy clienta...");
            return;
        }

        //informowanie o udaniu sie polaczenia
        Console.log("Połączenie z serwerem udane...");

        try{

            //tworzenie glownego systemu in / out
            this.mainIn = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
            this.mainOut = new PrintWriter(this.client.getOutputStream(), true);

        }catch (Exception e){

            //error
            Console.logError("Nieudało się stworzyć systemu IN / OUT controllera");
            Console.log("Kończenie pracy clienta...");
            return;
        }

        //odpalanie funkcji pobierajacej nick jak nick bedzie zatwierdzony przez server startuja watki do in/out
        if(nick()){

            //nick zaakceptowany przez serwer
            Console.logError("Gratulacje dołączyłeś do chatu od teraz możesz pisać :)");
            Console.logError("Aby wyłączyć aplikacje napisz /exit");

            //start watków (start())
            this.start();

        }else{

            //error
            Console.logError("Problemy z nickiem spróbuj ponownie potem!");
            Console.log("Kończenie pracy clienta...");
            return;
        }



    }

    //funkcja pobieraja nick i wysylajaca go
    public boolean nick(){

        //odpalenie pobierania nicku
        Console.log("Pobieranie nicku");

        //flaga nicku
        boolean nickFlag = true;

        //petla przerwie sie jak serwer zaakceptuje
        while(nickFlag){

            try {
                this.buffer = this.mainIn.readLine();
            } catch (Exception e) {

                //w przypadku gdy server nie dziala
                Console.logError("Utracono połączenie z serverem!");
                return false;
            }

            //sprawdzanie buffera
            if ((!this.buffer.equals(""))&&(!this.buffer.equals(null))){

                //sprawdzanie informacji od serwera
                if(this.buffer.equals("[SERVER] Podaj nick!")){

                    //pokazywanie wiadomosci od serwera na czerwono :)
                    Console.logError(buffer);

                    //pobieranie od usera nicku
                    buffer = console.next();

                    //wysylanie nicku
                    this.mainOut.println(buffer);

                } else if (this.buffer.equals("Gratulacje dołączyłeś do chatu :)")){
                    //wylaczamy petle :) i dajemy userowi dostep od chatu
                    nickFlag = false;

                    //zwracamy prawde
                    return true;
                }
            }
        }

        //zwracamy false cos ma raka
        return false;
    }

    //funkcja startujaca watki
    public void start(){

        //start watku dla wejscia
        this.clientIn = new ClientThread(this.client,true);
        this.clientIn.start();

        //start watku dla wyjscia
        this.clientOut = new ClientThread(this.client,false);
        this.clientOut.start();

        //petla dzialajaca w nieskonczonosc aby program nie zdechl
        while(this.status){}
    }

    //zamykanie client controllera
    public void closeController() {

        status = false;

        try{
            //wylaczanie clienIn
            this.clientIn.close();
        }catch (Exception e){}

        //nullowanie watku
        this.clientIn = null;

        try{
            //wylaczanie clientaIn
            this.clientOut.close();
        }catch (Exception e){}

        //nullowanie watku
        this.clientOut = null;

        try{
            //wylaczanie clienta
            this.client.close();
            //nullowanie socketa
            this.client = null;
        }catch (Exception e){}

        //informacja
        Console.log("Wyłączanie clienta...");
    }

    //klasa watku
    private class ClientThread extends Thread{

        //socket polaczenia
        private Socket conn;

        //obiekty do obslugi połaczenia
        private BufferedReader in;
        private PrintWriter out;

        //buffer
        private String bufferIn;
        private String bufferOut;

        //type
        private boolean type;

        //status
        private boolean status;

        //konstruktor
        //type to zmienna co mówy czy wątek dziala w trybie in czy out
        //in to true
        //out to flase
        public ClientThread(Socket socket,boolean type){

            //przekazywanie danych polaczenia
            this.conn = socket;

            //ustawianie typu
            this.type = type;

            //ustawianie statusu
            this.status = true;

            //tworzenie in/out
            try{
                //tworzenie obiektów in/out
                in = new BufferedReader(new InputStreamReader(this.conn.getInputStream()));
                out = new PrintWriter(this.conn.getOutputStream(), true);

            }catch (Exception e){

                //error
                Console.logError("System in/out dla wątku nie został zrobiony!");
                return;
            }

            //zerowanie bufferow
            this.bufferIn = "";
            this.bufferOut = "";
        }

        //funkcja run
        @Override
        public void run() {

            while (this.status) {

                //wykonywanie zadan w zaleznosci od typu
                if (this.type) {

                    //czesc dla in

                    //pobieranie od usera
                    this.bufferIn = console.nextLine();

                    if (this.bufferIn.equals("/exit")) {

                        //lamanie petli
                        break;
                    }

                    //wysylanie do servera
                    this.out.println(this.bufferIn);

                    //czysczenie
                    this.bufferIn = "";

                } else {

                    //czesc dla out

                    try {

                        //pobieranie wiadomosci
                        this.bufferOut = in.readLine();

                    } catch (Exception e) {

                        //error
                        if(status != false) Console.logError("Utracono połączenie z serverem!");
                        break;
                    }

                    //sprawdzanie wiadomosci
                    if ((!this.bufferOut.equals("")) && (!this.bufferOut.equals(null))) {

                        //pokazywanie wiadomosci
                        if(this.bufferOut.startsWith("[SERVER]")) Console.logError(this.bufferOut);
                        else Console.log(this.bufferOut);
                    }
                }
            }
            // czesc odpowiedzialna za wylaczanie
            closeController();
        }

        //funkcja close
        public void close(){
            this.status = false;
            //zamykanie in
            try {
                if (in != null) in.close();
            } catch (Exception e) {
            }
            //zamykanie out
            try {
                if (out != null) out.close();
            } catch (Exception e) {
            }
            //zamykanie socketa
            try {
                if (this.conn != null) this.conn.close();
            } catch (Exception e) {
            }

            //zamykanie watku
            this.interrupt();
        }
    }
}