/*
// ==================================================== //
//                    Copyright 2016                    //
//               Franciszek "Fgame" Olejnik             //
// ==================================================== //
// This work is licensed under a Creative Commons       //
//        Attribution 4.0 International License.        //
// ==================================================== //
//    Niniejsza praca jest licencjonowana zgodnie z     //
//       miedzynarodową licencja Creative Commons       //
//                "Uznanie autorstwa 4.0"               //
// ==================================================== //
*/

public class Console {

    //kolory dostępne w konsoli

    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_BLACK = "\u001B[30m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_PURPLE = "\u001B[35m";
    private static final String ANSI_CYAN = "\u001B[36m";
    private static final String ANSI_WHITE = "\u001B[37m";

    //aktualny kolor
    private static String color = ANSI_WHITE;

    //pisanie na ekranie
    public static void log(String text)
    {
        System.out.print(color);
        System.out.print(text);
        System.out.print(ANSI_RESET);
        System.out.print("\n");
    }

    //pokazywanie errora
    public static void logError(String text)
    {
        System.out.print(ANSI_RED);
        System.out.print(text);
        System.out.print(ANSI_RESET);
        System.out.print("\n");
    }

    //reset koloru
    public static void resetColor()
    {
        color = ANSI_WHITE;
    }

    //ustawianie nowego koloru
    public static void setColor(String name)
    {
        switch(name.toLowerCase())
        {
            case "red":
                color = ANSI_RED;
                break;
            case "green":
                color = ANSI_GREEN;
                break;
            case "yellow":
                color = ANSI_YELLOW;
                break;
            case "blue":
                color = ANSI_BLUE ;
                break;
            case "purple":
                color = ANSI_PURPLE;
                break;
            case "cyan":
                color = ANSI_CYAN;
                break;
            case "white":
                color = ANSI_WHITE;
                break;
            case "black":
                color = ANSI_BLACK;
                break;
            default:
                color = ANSI_WHITE;
                break;
        }
    }

}