/*
// ==================================================== //
//                    Copyright 2016                    //
//               Franciszek "Fgame" Olejnik             //
// ==================================================== //
// This work is licensed under a Creative Commons       //
//        Attribution 4.0 International License.        //
// ==================================================== //
//    Niniejsza praca jest licencjonowana zgodnie z     //
//       miedzynarodową licencja Creative Commons       //
//                "Uznanie autorstwa 4.0"               //
// ==================================================== //
*/

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class DataValidator {

    private static final String PORT_REGEX = "^0*(?:6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[1-5][0-9]{4}|[1-9][0-9]{1,3}|[0-9])$";

    public static boolean isPort(String port){
        if(port.matches(PORT_REGEX)) return true;
        else return false;
    }

    public static boolean isIp(String ip){
            if (ip.isEmpty()) {
                return false;
            }
            try {
                Object res = InetAddress.getByName(ip);
                return res instanceof Inet4Address || res instanceof Inet6Address;
            } catch (final UnknownHostException ex) {
                return false;
            }
    }

}