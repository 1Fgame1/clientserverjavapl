/*
// ==================================================== //
//                    Copyright 2016                    //
//               Franciszek "Fgame" Olejnik             //
// ==================================================== //
// This work is licensed under a Creative Commons       //
//        Attribution 4.0 International License.        //
// ==================================================== //
//    Niniejsza praca jest licencjonowana zgodnie z     //
//       miedzynarodową licencja Creative Commons       //
//                "Uznanie autorstwa 4.0"               //
// ==================================================== //
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;

public class ServerController {

    //zmienna status przyjmuje 0/1
    //1 dla dzialajacego servera
    //0 dla servera ktory ma zakonczyc prace
    private boolean status;

    //serverSocket
    private ServerSocket server;

    //zmienna na port
    private int port;

    //zmienna na ip
    private String ip;

    //tablica na watki
    private ArrayList<ServerClientThread> clients;

    //nicki userow
    private HashSet names;

    //id dla usera
    private int id;

    //konstruktor
    public ServerController(int port){
        this.clients = new ArrayList<ServerClientThread>();
        this.names = new HashSet<String>();
        this.id = 0;

        this.status = true;
        this.port = port;
    }

    //funkcja odpalajaca server
    public void start(){

        try{

            //tworzenie serverSocketa
            server = new ServerSocket(this.port);

            //pokazywanie wiadomosci o czekaniu na userow
            Console.log("Oczekiwanie na userów..");

            //glowna petla servera
            while(this.status){

                //gdy user poprosi o wylaczenie servera
                //(zabezpieczenie petla i tak nie powinna sie wykonac bo warunek musi dawac true )
                if(this.status == false){
                    break;
                }

                //pobieranie danych polaczenia
                Socket conn = this.server.accept();

                //tworzenie watku dla clienta
                ServerClientThread clientT = new ServerClientThread(conn,this.id);

                ++this.id;

                //dodawanie watku do tablicy
                this.clients.add(clientT);

                //odpalanie watku
                clientT.start();

            }

            //czesc odpowiedzialna za wylaczanie servera
            try{

                //funkcja wylaczajaca server
                close();

            }catch(Exception e){

                //error
                Console.logError("Wystąpił błąd podczas wyłączania servera!");
                Console.log("Aplikacja kończy pracę...");
                return;
            }

        }catch(Exception e){

            //error
            Console.logError("Nieudało się stworzyć servera!");
            Console.log("Aplikacja kończy pracę...");
            return;
        }
    }

    //funkcja wylaczajaca server
    public void close(){

    }

    //pisanie do wszystkich userów
    public synchronized void sayAll(String text){
        for(ServerClientThread client : this.clients){

            //pisanie do usera
            client.say(text);

        }
    }

    //serverText
    public String serverText(String text){
        return "\u001B[31m" + "[SERVER] " + text + "\u001B[0m";
    }

    //kasowanie usera z tablicy
    public void removeClient(String name,int id){

        //kasowanie imienia
        if (this.names.contains(name)) this.names.remove(name);

        for(int i = 0; i < this.clients.size() ; i++ ){
            ServerClientThread x = this.clients.get(i);

            if(x.getClientId() == id){
                this.clients.remove(i);
                //informacja
                Console.log("User o id: "+id+" został wykasowany z systemu!");
                sayAll("[SERVER] User o nicku "+name+" opuścił chat!");
                return;
            }
        }
    }


    //classa watku
    private class ServerClientThread extends Thread{

        //socket polaczenia
        private Socket conn;

        //id clienta
        private int clientId;

        //status
        private boolean status;

        //nick clienta
        private String nick;

        //obiekty do obslugi połaczenia
        private BufferedReader in;
        private PrintWriter out;

        //buffer
        private String buffer;

        //konstruktor
        public ServerClientThread(Socket socket,int clientId){

            //przekazywanie danych do watku z konstruktora
            this.conn = socket;
            this.clientId = clientId;

            //inicjacia nicku i buffera
            this.nick = "";
            this.buffer = "";

            this.status = true;

            //tworzenie systemu in/out
            try{

                //tworzenie obiektów in/out
                in = new BufferedReader(new InputStreamReader(this.conn.getInputStream()));
                out = new PrintWriter(this.conn.getOutputStream(), true);

            }catch(Exception e){

                //error
                Console.logError("System in/out dla clienta " + this.clientId + " nie został zrobiony!");
            }

            //popranie stworzone polaczenie
            Console.log("Client " + clientId + " dołączył " + socket);

        }

        //pobieranie id
        public int getClientId(){return this.clientId;}

        //pisanie do usera
        public void say(String text){
            out.println(text);
        }

        //funkcja zamykajaca wątek / polaczenie
        public void close() {
            //zamykanie in
            try {
                in.close();
            } catch (Exception e) {
            }
            //zamykanie out
            try {
                out.close();
            } catch (Exception e) {
            }
            //zamykanie socketa
            try {
                this.conn.close();
            } catch (Exception e) {
            }

            //zamykanie watku
            this.status = false;
        }

        //glowna funkcja wątku
        @Override
        public void run(){
            while (this.status) {

                //sprawdzanie czy nick istnieje jak tak to user ma dostep do chatu
                if(this.nick == "") {

                    //petla do pobierania nicku
                    while (true) {

                        //wysylanie wiadomosci do usera
                        say("[SERVER] Podaj nick!");

                        try {

                            //pobieranie jego odpowiedzi
                            this.buffer = in.readLine();

                            //pokazywanie wiadomosci od usera
                            Console.log("Wiadomosc od usera "+ this.clientId +" (nick) to " + buffer);

                        } catch (Exception e) {

                            //lamanie petli user ma raka

                            this.status = false;
                            break;
                        }

                        //sprawdzanie czy nick jest w bazie
                        if (names.contains(this.buffer)) {

                            //informowanie usera ze nick jest zajety
                            say(serverText("Ktoś posiada już ten nick wybierz inny!"));

                        } else {

                            //dodawanie nicku do tablicy nicków
                            names.add(this.buffer);

                            //przypisywanie nicku do wątku
                            this.nick = this.buffer;

                            //informowanie userów ze podana osoba dołączyła do chatu
                            say("Gratulacje dołączyłeś do chatu :)");
                            sayAll(serverText("Client o nicku: " + this.nick + " dołączył do chatu!"));
                            //konczenie pętli
                            break;
                        }
                    }
                }else{

                    //gdy nick zostal zaakceptowany
                    while(true){

                        //petla do przekazywania wiadomosci
                        try {

                            //pobieranie od  usera
                            this.buffer = this.in.readLine();

                        } catch (Exception e){

                            //w przypadku jak user sie rozłączy
                            //zamykanie watku
                            this.status = false;

                            //lamanie petli
                            break;
                        }

                        try{

                            //sprawdzanie wiadomosci
                            if ((!this.buffer.equals(""))&&(this.buffer.length() > 1)){

                                //jezeli user chce wylaczyc sie
                                if(this.buffer == "/exit"){

                                    //niszczenie petli
                                    break;

                                }else{
                                    //pokazywanie u servera wiadomosci
                                    Console.log("["+this.nick+"] "+this.buffer);

                                    //wysylanie do innych
                                    sayAll("["+this.nick+"] "+this.buffer);
                                }
                            }

                        }catch (Exception e){

                            //informowanie
                            Console.logError("Wystapil problem z userem: "+this.clientId);

                            //zamykanie watku
                            this.status = false;

                            //lamanie petli
                            break;
                        }
                    }
                }
            }
            //w przypadku jak user sie rozłączy
            Console.log("Client " + this.clientId + " rozłączył się!");

            //zamykanie watku
            this.close();

            //kasowanie watku
            if(this.nick.equals("")) removeClient("",this.clientId);
            else removeClient(this.nick,this.clientId);
        }
    }
}